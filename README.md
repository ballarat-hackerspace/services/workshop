# workshop

Administrative repository for all workshop related functions.

If you are here, you probably wanted the wiki, which is [over here](https://gitlab.com/ballarat-hackerspace/services/workshop/-/wikis/home)